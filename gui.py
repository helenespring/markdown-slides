import wx
import wx.html2
import markdown2
import codecs
import pathlib
import os
import datetime

class Input(wx.Frame):
    """ We simply derive a new class of Frame. """
    def __init__(self, parent, title, pos, size, name, input_file=None):
        wx.Frame.__init__(self, parent, title=title, pos = pos, size=size, name = name)
        self.control = wx.TextCtrl(self, style=wx.TE_MULTILINE | wx.TE_PROCESS_ENTER | wx.TE_WORDWRAP)
        self.currentDirectory = os.getcwd()

        #check if this file already exists
        if input_file:
            f = open(input_file, 'r')
            self.control.SetValue(f.read())
            f.close()
            #check if we need to add it to the file history upon saving
            self.save_as_flag = False
            self.path = input_file
            from file_history import files as files_dict
            try:
                self.settings_list = files_dict[self.path][1:]
            except KeyError:
                self.settings_list = None
        else:
            self.save_as_flag = True
            self.path = None # this is the path of the file
            self.settings_list = None

        self.CreateStatusBar()  # A Statusbar in the bottom of the window

        # Setting up the menu.
        filemenu = wx.Menu()
        helpmenu = wx.Menu()
        viewmenu = wx.Menu()
        editmenu = wx.Menu()

        # wx.ID_ABOUT and wx.ID_EXIT are standard IDs provided by wxWidgets.
        # the descriptor of the action shows up at the bottom of the frame.
        menu_about = filemenu.Append(wx.ID_ABOUT, "&About", " Information about this program")
        filemenu.AppendSeparator()
        menu_exit = filemenu.Append(wx.ID_EXIT, "E&xit", " Terminate the program")
        menu_open = filemenu.Append(wx.ID_OPEN, "&Open", "Open up a file")
        menu_save = filemenu.Append(wx.ID_SAVE, "&Save", "Save current file")
        menu_export = filemenu.Append(wx.ID_ANY, "&Export", "Export to HTML")

        self.Bind(wx.EVT_MENU, self.OnAbout, menu_about)
        self.Bind(wx.EVT_MENU, self.OnExit, menu_exit)
        self.Bind(wx.EVT_CLOSE, self.OnExit)
        self.Bind(wx.EVT_MENU, self.OnOpen, menu_open)
        self.Bind(wx.EVT_MENU, self.OnSave, menu_save)
        self.Bind(wx.EVT_MENU, self.OnExport, menu_export)

        #do something when text is entered
        self.Bind(wx.EVT_TEXT_ENTER, self.InsertEnter)
        self.Bind(wx.EVT_TEXT, self.Update)
        #also update text when opening a window
        self.Bind(wx.EVT_WINDOW_CREATE, self.Update)

        menu_help = helpmenu.Append(wx.ID_HELP, "help")

        self.Bind(wx.EVT_MENU, self.OnAbout, menu_help)

        menu_single_view = viewmenu.Append(wx.ID_ANY, "&Single view", "View individual slides")
        menu_multi_view = viewmenu.Append(wx.ID_ANY, "&Multi view (CTRL+/)", "View slide grid")
        menu_presentation_view = viewmenu.Append(wx.ID_ANY, "&Toggle presentation view (CTRL+P)", "Start/end presentation")

        menu_edit_slides = editmenu.Append(wx.ID_ANY, "&Slide appearance", "Edit slide template")

        self.Bind(wx.EVT_MENU, self.SingleView, menu_single_view)
        self.Bind(wx.EVT_MENU, self.MultiView, menu_multi_view)
        self.Bind(wx.EVT_MENU, self.PresentationView, menu_presentation_view)

        self.Bind(wx.EVT_MENU, self.SlideCustomization, menu_edit_slides)

        # Creating the menubar.
        menuBar = wx.MenuBar()
        menuBar.Append(filemenu, "&File")  # Adding the "filemenu" to the MenuBar
        menuBar.Append(editmenu, "&Edit") #
        menuBar.Append(viewmenu, "&View") # Change view functions in the output
        menuBar.Append(helpmenu, "&Help")  #
        self.SetMenuBar(menuBar)  # Adding the MenuBar to the Frame content.
        self.Show(True)

        #Add a shortcut for saving files: ctrl + s
        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL, ord('S'), wx.ID_SAVE)])
        self.SetAcceleratorTable(accel_tbl)

        #store initial save
        self.last_saved = self.control.GetValue()

        #store truncated text for use in single view output
        self.trunk = str()

    def OnExport(self, e):
        child = wx.FindWindowByName('bowser')
        if child:
            child.ExportSlides(e)

    def SingleView(self, e):
        child = wx.FindWindowByName('bowser')
        if child:
            child.DisplaySingleView(e)

    def MultiView(self, e):
        child = wx.FindWindowByName('bowser')
        if child:
            child.DisplayMultiView(e)

    def PresentationView(self, e):
        child = wx.FindWindowByName('bowser')
        if child:
            child.DisplayPresentationView(e)

    def SlideCustomization(self, e):
        child = wx.FindWindowByName('bowser')
        if child:
            child.CustomizeSlides(e)

    def OnSave(self, e):
        if self.save_as_flag:
            #1) save the file
            #getting info
            wildcard = "Markdown (*.md)|*.md"
            dlg = wx.FileDialog(
                self, message="Save file as ...",
                defaultDir=self.currentDirectory,
                defaultFile="", wildcard=wildcard, style=wx.FD_SAVE
            )
            if dlg.ShowModal() == wx.ID_OK:
                path = dlg.GetPath() #now we have the path of the file
            dlg.Destroy()
            #save here
            contents = self.control.GetValue()
            filehandle = open(path, 'w')
            filehandle.write(contents)
            filehandle.close()
            self.save_as_flag = False
            self.path = path
            # update path in output
            child.update_path(self.path)
        else:
            #just overwrite the current contents of the existing file.
            contents = self.control.GetValue()
            filehandle = open(self.path, 'w')
            filehandle.write(contents)
            filehandle.close()
        # 2) register with file_history
        from file_history import files as files_dict
        # if self.path not in set(files_dict.keys()):
        # add it
        # need information from child
        child = wx.FindWindowByName('bowser')
        files_dict[self.path] = [datetime.datetime.now().strftime("%Y-%m-%d, %H-%M-%S"),
                            child.font_style, child.font_size, child.slide_color, child.text_color]
        files = open('file_history.py', 'w')
        files.write('files = ' + str(files_dict))
        #update
        self.last_saved = self.control.GetValue()

    def InsertEnter(self,e):
        #kind of annoying to have the <br> pop up physically.
        #I was really sure that I had managed to insert \n into the text itself
        #but i can't access that anymore! better push anyway.
        self.control.WriteText('\n')

    def Update(self,e):
        #get the child window
        text = self.control.GetValue()
        self.trunk = text[:self.control.GetInsertionPoint()]
        #find which slide we are editing
        slide_num = self.trunk.count('===')
        #send to output
        child = wx.FindWindowByName('bowser')
        if child:
            child.UpdateText(text, slide_num)
            if self.settings_list:
                child.set_slideview(self.settings_list)
                #only do this once, at the beginning
                self.settings_list = None

    def OnAbout(self, e):
        # A message dialog box with an OK button. wx.OK is a standard ID in wxWidgets.
        # order of text in next line is body - title. last entry adds a button
        dlg = wx.MessageDialog(self, "Welcome to Splash! Check out the github repository.",
                               "About Splash", wx.OK)
        dlg.ShowModal()  # Show it
        dlg.Destroy()  # finally destroy it when finished.

    def OnOpen(self, e):
        """ Open a file"""
        self.dirname = ''
        dlg = wx.FileDialog(self, "Choose a file", self.dirname, "", "*.*", wx.FD_OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            self.filename = dlg.GetFilename()
            self.dirname = dlg.GetDirectory()
            f = open(os.path.join(self.dirname, self.filename), 'r')
            self.control.SetValue(f.read())
            f.close()
        dlg.Destroy()
        path = dlg.GetPath()
        from file_history import files as files_dict
        if path not in set(files_dict.keys()):
            child = wx.FindWindowByName('bowser')
            # add it
            files_dict[path] = [datetime.datetime.now().strftime("%Y-%m-%d, %H-%M-%S"),
                            child.font_style, child.font_size, child.slide_color, child.text_color]
            files = open('file_history.py', 'w')
            files.write('files = ' + str(files_dict))
        self.save_as_flag = False
        self.path = dlg.GetPath()
        child.update_path(self.path)

    def OnExit(self, e):
        latest_text = self.control.GetValue()
        if (latest_text != self.last_saved) or (self.path == None):
            # warn that there are unsaved changes
            dlg = wx.MessageDialog(self, "Discard unsaved changes & exit?",
            "Warning: unsaved changes", wx.OK | wx.CANCEL)
            if dlg.ShowModal() != wx.ID_OK:
                dlg.Destroy()
            else:
                dlg.Destroy()
                launch = Launch(None, (575, 575), name='launch')
                self.Destroy()
        else:
            launch = Launch(None, (575, 575), name='launch')
            self.Destroy()





class Output(wx.Dialog):
    def __init__(self, parent, pos, size, name):
        wx.Dialog.__init__(self, parent = parent, name = name, pos = pos,
                           style =  wx.MAXIMIZE_BOX | wx.RESIZE_BORDER)

        self.browser = wx.html2.WebView.New(self)
        self.SetSize(size)

        self.browser.SetPage('', "")
        self.Show()
        self.Bind(wx.EVT_CLOSE, self.OnExit)

        #get current working directory
        if parent.path:
            self.path_cwd = parent.path[:parent.path.rfind('/')]
        else:
            self.path_cwd = None

        self.html_text = codecs.open("html-stuff.html", 'r').read()
        self.single_view_html_text = codecs.open("single_view_html.html", 'r').read()
        self.fullscreen_view_html_text = codecs.open("fullscreen_view_html.html", 'r').read()
        #
        self.text = str()
        self.frame_no = None

        #Recall size and pos
        self.size = size
        self.pos = pos

        #record template details
        self.slide_color = '#fff' #pale gray
        self.font_style = 'Arial, Helvetica, sans-serif' #font type
        self.font_size = 1 #proportionality constant
        self.text_color = '#000' #black

        #make a variable to show fullscreen
        self.show_fullscreen = False

        #link clicking on slides to switching the view
        self.Bind(wx.html2.EVT_WEBVIEW_NAVIGATING, self.OnLink, self.browser)

        #link right and left keys to navigation, and for returning to multiview
        ac = []
        keys = [(wx.ACCEL_NORMAL, wx.WXK_LEFT, self.SwitchSlideLeft),
                (wx.ACCEL_NORMAL, wx.WXK_RIGHT, self.SwitchSlideRight),
                (wx.ACCEL_CTRL, ord('/'), self.DisplayMultiView),
                (wx.ACCEL_CTRL, ord('P'), self.ToggleFullscreen)]
        for key in keys:
            _id = wx.NewIdRef()
            ac.append( (key[0], key[1], _id) )
            self.Bind(wx.EVT_MENU, key[2], id=_id)
        tbl = wx.AcceleratorTable(ac)
        self.SetAcceleratorTable(tbl)

    def update_path(self, path):
        #if a new file is saved or the current working directory is changed in the input, update the path in the output
        self.path_cwd = path

    def ExportSlides(self,e):
        #fullscreen
        export_html = self.html_text.replace('47.5vmax', '100vmax')
        export_html = export_html.replace('repeat(2, 1fr)', '1fr')
        #run again
        export_html = self.markdown_to_html(export_html, self.text)
        #remove links
        export_html = export_html.replace('<a', '<div')
        export_html = export_html.replace('</a', '</div')
        #
        wildcard = "HTML (*.html)|*.html"
        self.currentDirectory = os.getcwd()
        dlg = wx.FileDialog(
            self, message="Save file as ...",
            defaultDir=self.currentDirectory,
            defaultFile="", wildcard=wildcard, style=wx.FD_SAVE
        )
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()  # now we have the path of the file
        dlg.Destroy()
        # save here
        filehandle = open(path, 'w')
        filehandle.write(export_html)
        filehandle.close()

    def set_slideview(self,settings_list):
        [font_style, font_size, slide_color, text_color] = settings_list
        self.font_style = font_style
        self.font_size = font_size
        self.slide_color = slide_color
        self.text_color = text_color
        self.UpdateCSS()

    def UpdateCSS(self):
        multi = codecs.open("html-stuff.html", 'r').read()
        single = codecs.open("single_view_html.html", 'r').read()
        fullscreen = codecs.open("fullscreen_view_html.html", 'r').read()

        #multi
        multi = multi.replace('font-size: 1vmax', 'font-size: {0}vmax'.format(self.font_size*1))
        multi = multi.replace('font-size: 0.75vmax', 'font-size: {0}vmax'.format(self.font_size*0.75))
        multi = multi.replace('background: #fff', 'background: {0}'.format(self.slide_color))
        multi = multi.replace('color: #000', 'color: {0}'.format(self.text_color))
        multi = multi.replace('font-family: Arial, Helvetica, sans-serif', 'font-family: {0}'.format(self.font_style))

        #single and fullscreen
        single = single.replace('font-size: 2vmax', 'font-size: {0}vmax'.format(self.font_size * 2))
        single = single.replace('font-size: 1.5vmax', 'font-size: {0}vmax'.format(self.font_size * 1.5))
        single = single.replace('background: #fff', 'background: {0}'.format(self.slide_color))
        single = single.replace('color: #000', 'color: {0}'.format(self.text_color))
        single = single.replace('font-family: Arial, Helvetica, sans-serif', 'font-family: {0}'.format(self.font_style))

        fullscreen = fullscreen.replace('font-size: 2vmax', 'font-size: {0}vmax'.format(self.font_size * 2))
        fullscreen = fullscreen.replace('font-size: 1.5vmax', 'font-size: {0}vmax'.format(self.font_size * 1.5))
        fullscreen = fullscreen.replace('background: #fff', 'background: {0}'.format(self.slide_color))
        fullscreen = fullscreen.replace('color: #000', 'color: {0}'.format(self.text_color))
        fullscreen = fullscreen.replace('font-family: Arial, Helvetica, sans-serif', 'font-family: {0}'.format(self.font_style))

        #set
        self.html_text = multi
        self.single_view_html_text = single
        self.fullscreen_view_html_text = fullscreen
        self.export_html = fullscreen

        #update view
        self.UpdateText(self.text, self.frame_no)

    def CustomizeSlides(self, e):
        #launch a dialog
        dlg = wx.Dialog(parent = None, pos = (300,300), size = (200,200),
                           style =  wx.MAXIMIZE_BOX | wx.CLOSE_BOX)

        color_button = wx.Button(dlg, -1, 'Slide color')
        text_color_button = wx.Button(dlg, -1, 'Text color')
        font_style_button = wx.Button(dlg, -1, 'Font style')
        font_size_button = wx.Button(dlg, -1, 'Font sizes')

        #activate
        color_button.Bind(wx.EVT_BUTTON, self.color_dialog)
        text_color_button.Bind(wx.EVT_BUTTON, self.text_color_dialog)
        font_style_button.Bind(wx.EVT_BUTTON, self.font_style_dialog)
        font_size_button.Bind(wx.EVT_BUTTON, self.font_size_dialog)
        #
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(color_button, 0, wx.CENTER|wx.ALL,0)
        sizer.Add(text_color_button, 0, wx.CENTER|wx.ALL,0)
        sizer.Add(font_style_button, 0, wx.CENTER|wx.ALL,0)
        sizer.Add(font_size_button, 0, wx.CENTER|wx.ALL,0)
        sizer.SetSizeHints(dlg)
        dlg.SetSizer(sizer)
        #connect buttons to actions

        dlg.ShowModal()  # Show it
        dlg.Destroy()  # finally destroy it when finished.

    def color_dialog(self, e):
        dlg = wx.ColourDialog(self)
        dlg.GetColourData().SetChooseFull(True)
        if dlg.ShowModal() == wx.ID_OK:
            colourData = dlg.GetColourData()
            color = colourData.GetColour()
            color_tuple = (color.Red(), color.Green(), color.Blue())
            hex_color = '#%02x%02x%02x' % color_tuple
            self.slide_color = hex_color
            self.UpdateCSS()
        dlg.Destroy

    def text_color_dialog(self, e):
        dlg = wx.ColourDialog(self)
        dlg.GetColourData().SetChooseFull(True)
        if dlg.ShowModal() == wx.ID_OK:
            data = dlg.GetFontData()
            font = data.GetChosenFont()
            colour = data.GetColour()

            self.log.WriteText('You selected: "%s", %d points, color %s\n' %
                               (font.GetFaceName(), font.GetPointSize(),
                                colour.Get()))

            self.curFont = font
            self.curClr = colour
            self.UpdateUI()

            # Don't destroy the dialog until you get everything you need from the
            # dialog!
            dlg.Destroy()

    def font_style_dialog(self, e):
        dlg = wx.SingleChoiceDialog(None, "Font families", "Font picker",
                                    ['Arial, Helvetica, sans-serif',
                                     '"Times New Roman", Times, serif',
                                     'Georgia, serif',
                                     '"Palatino Linotype", "Book Antiqua", Palatino, serif',
                                     '"Times New Roman", Times, serif'],
                                    wx.CHOICEDLG_STYLE)
        if dlg.ShowModal() == wx.ID_OK:
            self.font_style = dlg.GetStringSelection()
            self.UpdateCSS()
            dlg.Destroy

    def font_size_dialog(self, e):
        dlg = wx.SingleChoiceDialog(None, "Proportionality constant (default: 1)", "Font size",
                                    ['0.5',
                                     '1',
                                     '1.5',
                                     '2',
                                     '2.5',
                                     '3'],
                                    wx.CHOICEDLG_STYLE)
        if dlg.ShowModal() == wx.ID_OK:
            self.font_size = float(dlg.GetStringSelection())
            self.UpdateCSS()
            dlg.Destroy

    def SwitchSlideLeft(self, e):
        if self.frame_no:
            if self.frame_no > 1:
                self.frame_no = self.frame_no - 1
            else:
                self.frame_no = self.frame_no
            if self.show_fullscreen:
                html_text = self.markdown_to_html(self.fullscreen_view_html_text, self.text)
                self.browser.SetPage(html_text, "")
            else:
                html_text = self.markdown_to_html(self.single_view_html_text, self.text)
                self.browser.SetPage(html_text, "")

    def SwitchSlideRight(self, e):
        if self.frame_no:
            self.frame_no = self.frame_no + 1 #relegate check for max slide reached
            #to markdown_to_html
            if self.show_fullscreen:
                html_text = self.markdown_to_html(self.fullscreen_view_html_text, self.text)
                self.browser.SetPage(html_text, "")
            else:
                html_text = self.markdown_to_html(self.single_view_html_text, self.text)
                self.browser.SetPage(html_text, "")

    def ToggleFullscreen(self, e):
        #only do something if we're in single slide view
        if self.frame_no:
            if self.show_fullscreen == True:
                #switch
                self.show_fullscreen = False
                self.DisplaySingleView(e)
            else:
                self.show_fullscreen = True
                self.DisplayPresentationView(e)

    def DisplaySingleView(self, e):
        #disable full screen
        self.ShowFullScreen(False)
        #show the first slide
        self.frame_no = 1 # will be converted to 0 l8er
        # change aspect ratio
        self.SetSize((self.size[0], self.size[0] * 9 / 16))
        html_text = self.markdown_to_html(self.single_view_html_text, self.text)
        self.browser.SetPage(html_text, "")

    def DisplayMultiView(self, e):
        self.frame_no = None
        html_text = self.markdown_to_html(self.html_text, self.text)
        self.browser.SetPage(html_text, "")

    def DisplayPresentationView(self, e):
        self.frame_no = 1
        # self.Maximize()
        self.ShowFullScreen(True)
        html_text = self.markdown_to_html(self.fullscreen_view_html_text, self.text)
        self.browser.SetPage(html_text, "")

    def OnLink(self, e):
        target_frame = e.GetURL()[7:]
        if target_frame != '/':
            self.frame_no = int(target_frame[1:])+1
            #change aspect ratio
            self.SetSize( (820, 480) )

            html_text = self.markdown_to_html(self.single_view_html_text, self.text)
            self.browser.SetPage(html_text, "")

    def OnExit(self, e):
        #take the parent down with it
        launch = Launch(None, (575, 575), name='launch')
        parent = wx.FindWindowByName('frame')
        parent.Destroy()
        self.Destroy()

    def UpdateText(self, text, slide_num):
        if self.frame_no:
            self.frame_no = slide_num + 1
            html_ = self.single_view_html_text
        else:
            html_ = self.html_text
        self.text = text
        html_text = self.markdown_to_html(html_,text)
        self.browser.SetPage(html_text,"")

    def citation_to_suptext(self, html, start_footnote_pos, end_footnote_pos, end_bracket_pos):
        if end_bracket_pos == None:
            end_bracket_pos = end_footnote_pos
        return html[:start_footnote_pos] + '<sup>' + \
                                   html[start_footnote_pos+2:start_footnote_pos+3] + '</sup>' + html[end_bracket_pos+1:]

    def separate_text(self, md):
        # if there's a header, send it to header container in the grid
        if '<h1>' in md and '</h1>' in md:
            start_title = md.find('<h1>')
            end_title = md.find('</h1>')
            title = md[start_title + 4:end_title]
            header = md[:start_title]
            main_text = md[end_title + 5:]
        else:
            title = ''
            header = ''
            main_text = md

        # if there are footers, send them to the footer container in the grid
        if '[^' in main_text:
            #there could be multiple footnotes
            #it's better to collect all of them, so they can be listed in an arbitrary order
            citing_text = dict()
            for index in range(main_text.count('[^')):
                #find position of the footnote
                start_footnote_pos = main_text.find('[^')
                #grab the footnote wrapper and id
                end_footnote_pos = main_text[start_footnote_pos:].find(']')+start_footnote_pos
                footnote_num = main_text[start_footnote_pos+2:start_footnote_pos+3]
                if end_footnote_pos != -1: #found it
                    #does it have something to cite
                    if main_text[end_footnote_pos+1] == '(':
                        #it do
                        #check if the text is entered already
                        check_chunk = main_text[end_footnote_pos+2:]
                        first_close = check_chunk.find(')')
                        first_open = check_chunk.find('(')
                        if (first_close < first_open and first_close != -1) or (first_open == -1 and first_close != -1):
                            citing_text[footnote_num] = main_text[end_footnote_pos+2:first_close+end_footnote_pos+2]
                            main_text = self.citation_to_suptext(main_text, start_footnote_pos, end_footnote_pos, end_bracket_pos = first_close+end_footnote_pos+2)
                        else:
                            #we have to wait for it to be entered
                            continue
                    else:
                        #is it already a footnote with an associated text
                        if footnote_num in set(list(citing_text.keys())):
                            #just need to add a sup
                            main_text = self.citation_to_suptext(main_text, start_footnote_pos, end_footnote_pos, end_bracket_pos=None)
            footer_text = '<ul style="list-style-type:none;">'
            for footnote_n, text in citing_text.items():
                #make the footer text
                footer_text = footer_text + '<li>' + '[' + footnote_n + '] ' + text + '</li>'
            footer_text = footer_text + '</ul>'
        else:
            footer_text = ''
        #finally convert with markdown2
        return markdown2.markdown(header), markdown2.markdown(title), markdown2.markdown(main_text), markdown2.markdown(footer_text)


    def separate_text_and_image(self, html, l_or_r, index, div):

        header, title, main_text, footer = self.separate_text(html)

        if main_text.count('<img')==2:
            #there are two images
            text_before_image, first_image_and_between_text, second_image = main_text.split('<img')
            first_image, between_text = first_image_and_between_text.split('/>')
            second_image = second_image[:second_image.find('/>')+2]
            #has an extra <p>
            text_before_image = text_before_image + between_text
            first_image = '<img' + first_image + '/>'
            second_image = '<img' + second_image

            # dress the blocks directly into one
            block = '<{0} id="split_slide" href="{1}">'.format(div, index) + \
                        '<header>' + header + '</header>' + \
                        '<st>' + title + '</st>' + \
                        '<left>' + first_image + '</left>' + \
                        '<right>' + second_image + '</right>' + \
                    '</{0}>'.format(div)
        elif main_text.count('<img')==1:
            #only one image
            text_before_image, image_and_text = main_text.split('<img')
            image, text_after_image = image_and_text.split('alt="' + l_or_r + '" />')
            text_before_image = text_before_image + '</p>'
            image = '<img' + image + '/>'
            text_after_image = '<p>' + text_after_image
            #i want to exclude text if it includes a bunch of meaningless <br>
            if text_before_image.find('<br>') == 3:
                text_before_image = '<p>' + text_before_image[7:]
            if text_after_image.find('<br>') == 3:
                text_after_image = '<p>' + text_after_image[7:]
            #check if anything's left
            if len(text_before_image) == 7:
                if len(text_after_image) == 7:
                    text = ''
                else:
                    text = text_after_image
            else:
                text = text_before_image + text_after_image

            #dress the image and text blocks directly into one
            if l_or_r == 'left':
                block = '<{0} id="split_slide" href="{1}">'.format(div, index) + \
                              '<header>' + header + '</header>' + \
                              '<st>' + title + '</st>' + \
                              '<left>' + image + '</left>' + \
                              '<right>' + text + '</right>' + \
                        '</{0}>'.format(div)
            elif l_or_r == 'right':
                block = '<{0} id="split_slide" href="{1}">'.format(div, index) + \
                            '<header>' + header + '</header>' + \
                            '<st>' + title + '</st>' + \
                            '<right>' + image + '</right>' \
                                               '<left>' + text + '</left>' + \
                            '</{0}>'.format(div)
        else:
            raise RuntimeError('img not detected!')
        return block

    def markdown_to_html(self, html_text, text):

        # have to process headers ourselves
        split_text = text.split('===\n')
        for ind, text_chunk in enumerate(split_text):
            if '#' in text_chunk:
                # first enter should be precisely between the header and the body
                first_enter = text_chunk.find('\n')
                if first_enter != -1:
                    text_chunk = text_chunk.replace('#', '<h1>')
                    text_chunk = text_chunk.replace('\n', '</h1>\n', 1)
            split_text[ind] = text_chunk
        text = '===\n'.join(split_text)

        # replace \n with <br>
        text = text.replace('\n', '<br>')
        split_text = text.split('===<br>')

        #check what view we want
        if self.frame_no:
            div = 'div'
            #check if we have surpassed max length by navigating
            if self.frame_no >= len(split_text):
                self.frame_no = len(split_text)
            split_text = [split_text[self.frame_no-1]]
        else:
            div = 'a'

        for ind,chunk in enumerate(split_text):

            #have to check whether to make a left/right subcontainer
            #should be the first specification.
            if '![left]' in chunk:
                # put the image in the left and the text in the right
                block = self.separate_text_and_image(chunk, 'left', ind, div)
                html_text = html_text + block
            elif '![right]' in chunk:
                #do the opposite
                block = self.separate_text_and_image(chunk, 'right', ind, div)
                html_text = html_text + block
            else:
                header,title,main_text,footer = self.separate_text(chunk)
                html_text = html_text + '<{0} id="slide" href="{1}">'.format(div, ind) + \
                            '<header>' + header + '</header>' + \
                            '<st>' + title + '</st>' + \
                            '<main>' + main_text + '</main>' + \
                            '<footer>' + footer + '</footer>' + '</{0}> \n'.format(div)
        html_text = html_text + '</div>'
        #check if we need to add an image
        if 'src=' in html_text:
            # treat images by adding the full path
            if 'https:' not in html_text:
                html_text = html_text.replace('src="', 'src= "' + self.path_cwd + '/')
        # print(html_text)
        return html_text




class Launch(wx.Frame):
    def __init__(self, parent, size, name):
        wx.Dialog.__init__(self, parent = parent, name = name,
                           style =  wx.CLOSE_BOX )

        self.browser = wx.html2.WebView.New(self)
        self.SetSize(size)

        self.path_cwd = os.getcwd()

        # Setting up the menu.
        filemenu = wx.Menu()
        helpmenu = wx.Menu()

        # wx.ID_ABOUT and wx.ID_EXIT are standard IDs provided by wxWidgets.
        # the descriptor of the action shows up at the bottom of the frame.
        menu_about = filemenu.Append(wx.ID_ABOUT, "&About", " Information about this program")
        filemenu.AppendSeparator()
        menu_exit = filemenu.Append(wx.ID_EXIT, "E&xit", " Terminate the program")
        menu_open = filemenu.Append(wx.ID_OPEN, "&Open", "Open up a file")

        self.Bind(wx.EVT_MENU, self.OnAbout, menu_about)
        self.Bind(wx.EVT_MENU, self.OnExit, menu_exit)
        self.Bind(wx.EVT_MENU, self.OnOpen, menu_open)

        # Creating the menubar.
        menuBar = wx.MenuBar()
        menuBar.Append(filemenu, "&File")  # Adding the "filemenu" to the MenuBar
        menuBar.Append(helpmenu, "&Help")  # Adding the "filemenu" to the MenuBar
        self.SetMenuBar(menuBar)  # Adding the MenuBar to the Frame content.

        self.Bind(wx.EVT_CLOSE, self.OnExit)

        self.html_text = codecs.open("launch_html.html", 'r').read()[:-6]

        #what to call the slide types
        self.slide_type = []

        #look at file_history, and prepare the slide previews
        from file_history import files as files_dict
        #sort the dictionary from most to least recent modification
        files_dict = {k: v for k, v in sorted(files_dict.items(), key=lambda item: item[1], reverse = True)}
        for ind, path in enumerate(list(files_dict.keys())):
            try:
                filehandle = open(path, 'r')
            except FileNotFoundError:
                files_dict.pop(path)
                files = open('file_history.py', 'w')
                files.write('files = ' + str(files_dict))
                continue
            text_ = filehandle.read()
            # add a special class with appearance specifications for each file
            self.gen_html_text(self.html_text, list(files_dict.values())[ind][1:], ind, text_)
            text_ = self.launch_markdown_to_html(text_, path, self.slide_type[-1])
            self.html_text = self.html_text + text_
        text = self.html_text + '</div>'
        #now display the result
        self.browser.SetPage(text, "")
        self.Center()
        self.Show()

        # detecting navigating (clicking hyperlinks)
        #has to be placed here, otherwise will be triggered erroneously.
        self.Bind(wx.html2.EVT_WEBVIEW_NAVIGATING, self.OnLink, self.browser)


    def OnLink(self, e):
        #we can see what the action to take is by looking at the URL.
        target_path = e.GetURL()[7:]
        if target_path == "/new":
            #make a blank presentation: so, just open the editor.
            input_frame = Input(None, 'Markdown editor', (200, -500), (400, 250), name='frame', input_file = None)
            output_frame = Output(input_frame, pos=(720, 200), size=(800, 600), name='bowser')
            self.Destroy()
        else:
            #have to truncate it to remove the last character (is a 0 added to stop the link from working)
            # target_path = target_path[:-1]
            input_frame = Input(None, 'Markdown editor', (200, -500), (400, 250), name='frame', input_file=target_path)
            output_frame = Output(input_frame, pos=(720, 200), size=(800, 600), name='bowser')
            self.Destroy()

    def OnAbout(self, e):
        # A message dialog box with an OK button. wx.OK is a standard ID in wxWidgets.
        # order of text in next line is body - title. last entry adds a button
        dlg = wx.MessageDialog(self, "Splash",
                               "Here's a thing or two about Splash.", wx.OK)
        dlg.ShowModal()  # Show it
        dlg.Destroy()  # finally destroy it when finished.

    def OnOpen(self, e):
        """ Open a file"""
        self.dirname = ''
        dlg = wx.FileDialog(self, "Choose a file", self.dirname, "", "*.*", wx.FD_OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            self.filename = dlg.GetFilename()
            self.dirname = dlg.GetDirectory()
            path = os.path.join(self.dirname, self.filename)
            input_frame = Input(None, 'Markdown editor', (200, -500), (400, 250), name='frame', input_file=path)
            output_frame = Output(input_frame, pos=(720, 200), size=(800, 600), name='bowser')
            self.Destroy()
        dlg.Destroy()


    def OnExit(self, e):
        self.Destroy()

    def launch_citation_to_suptext(self, html, start_footnote_pos, end_footnote_pos, end_bracket_pos):
        if end_bracket_pos == None:
            end_bracket_pos = end_footnote_pos
        return html[:start_footnote_pos] + '<sup>' + \
                                   html[start_footnote_pos+2:start_footnote_pos+3] + '</sup>' + html[end_bracket_pos+1:]

    def launch_separate_text(self, md):
        # if there's a header, send it to header container in the grid
        if '<h1>' in md and '</h1>' in md:
            start_title = md.find('<h1>')
            end_title = md.find('</h1>')
            title = md[start_title + 4:end_title]
            header = md[:start_title]
            main_text = md[end_title + 5:]
        else:
            title = ''
            header = ''
            main_text = md

        # if there are footers, send them to the footer container in the grid
        if '[^' in main_text:
            #there could be multiple footnotes
            #it's better to collect all of them, so they can be listed in an arbitrary order
            citing_text = dict()
            for index in range(main_text.count('[^')):
                #find position of the footnote
                start_footnote_pos = main_text.find('[^')
                #grab the footnote wrapper and id
                end_footnote_pos = main_text[start_footnote_pos:].find(']')+start_footnote_pos
                footnote_num = main_text[start_footnote_pos+2:start_footnote_pos+3]
                if end_footnote_pos != -1: #found it
                    #does it have something to cite
                    if main_text[end_footnote_pos+1] == '(':
                        #it do
                        #check if the text is entered already
                        check_chunk = main_text[end_footnote_pos+2:]
                        first_close = check_chunk.find(')')
                        first_open = check_chunk.find('(')
                        if (first_close < first_open and first_close != -1) or (first_open == -1 and first_close != -1):
                            citing_text[footnote_num] = main_text[end_footnote_pos+2:first_close+end_footnote_pos+2]
                            main_text = self.launch_citation_to_suptext(main_text, start_footnote_pos, end_footnote_pos, end_bracket_pos = first_close+end_footnote_pos+2)
                        else:
                            #we have to wait for it to be entered
                            continue
                    else:
                        #is it already a footnote with an associated text
                        if footnote_num in set(list(citing_text.keys())):
                            #just need to add a sup
                            main_text = self.launch_citation_to_suptext(main_text, start_footnote_pos, end_footnote_pos, end_bracket_pos=None)
            footer_text = '<ul style="list-style-type:none;">'
            for footnote_n, text in citing_text.items():
                #make the footer text
                footer_text = footer_text + '<li>' + '[' + footnote_n + '] ' + text + '</li>'
            footer_text = footer_text + '</ul>'
        else:
            footer_text = ''
        #finally convert with markdown2
        return markdown2.markdown(header), markdown2.markdown(title), markdown2.markdown(main_text), markdown2.markdown(footer_text)


    def launch_separate_text_and_image(self, html, l_or_r, path):

        header, title, main_text, footer = self.launch_separate_text(html)

        if main_text.count('<img')==2:
            #there are two images
            text_before_image, first_image_and_between_text, second_image = main_text.split('<img')
            first_image, between_text = first_image_and_between_text.split('/>')
            second_image = second_image[:second_image.find('/>')+2]
            #has an extra <p>
            text_before_image = text_before_image + between_text
            first_image = '<img' + first_image + '/>'
            second_image = '<img' + second_image

            # dress the blocks directly into one
            block = '<a id="split_slide" href="{0}">'.format(path) + \
                        '<header>' + header + '</header>' + \
                        '<st>' + title + '</st>' + \
                        '<left>' + first_image + '</left>' + \
                        '<right>' + second_image + '</right>' + \
                    '</a>'
        elif main_text.count('<img')==1:
            #only one image
            text_before_image, image_and_text = main_text.split('<img')
            image, text_after_image = image_and_text.split('alt="' + l_or_r + '" />')
            text_before_image = text_before_image + '</p>'
            image = '<img' + image + '/>'
            text_after_image = '<p>' + text_after_image
            #i want to exclude text if it includes a bunch of meaningless <br>
            if text_before_image.find('<br>') == 3:
                text_before_image = '<p>' + text_before_image[7:]
            if text_after_image.find('<br>') == 3:
                text_after_image = '<p>' + text_after_image[7:]
            #check if anything's left
            if len(text_before_image) == 7:
                if len(text_after_image) == 7:
                    text = ''
                else:
                    text = text_after_image
            else:
                text = text_before_image + text_after_image

            #dress the image and text blocks directly into one
            if l_or_r == 'left':
                block = '<a id="split_slide" href="{0}">'.format(path) + \
                              '<header>' + header + '</header>' + \
                              '<st>' + title + '</st>' + \
                              '<left>' + image + '</left>' + \
                              '<right>' + text + '</right>' + \
                        '</a>'
            elif l_or_r == 'right':
                block = '<a id="split_slide" href="{0}">'.format(path) + \
                            '<header>' + header + '</header>' + \
                            '<st>' + title + '</st>' + \
                            '<right>' + image + '</right>' \
                                               '<left>' + text + '</left>' + \
                            '</a>'
        else:
            raise RuntimeError('img not detected!')
        return block

    def launch_markdown_to_html(self, text, path, slide_type):

        # have to process headers ourselves
        text_chunk = text.split('===\n')[0]
        if '#' in text_chunk:
            # first enter should be precisely between the header and the body
            first_enter = text_chunk.find('\n')
            if first_enter != -1:
                text_chunk = text_chunk.replace('#', '<h1>')
                text_chunk = text_chunk.replace('\n', '</h1>\n', 1)
        text = text_chunk

        # replace \n with <br>
        text = text.replace('\n', '<br>')

        chunk = text
        #have to check whether to make a left/right subcontainer
        #should be the first specification.
        if '![left]' in chunk:
            # put the image in the left and the text in the right
            block = self.launch_separate_text_and_image(chunk, 'left', path)
        elif '![right]' in chunk:
            #do the opposite
            block = self.launch_separate_text_and_image(chunk, 'right', path)
        else:
            header,title,main_text,footer = self.launch_separate_text(chunk)
            block = '<a id="{0}" href="{1}">'.format(slide_type, path) + \
                        '<header>' + header + '</header>' + \
                        '<st>' + title + '</st>' + \
                        '<main>' + main_text + '</main>' + \
                        '<footer>' + footer + '</footer>' + '</a> \n'
        if 'src=' in block:
            # treat images by adding the full path
            if 'https:' not in block:
                block = block.replace('src="', 'src= "'+self.path_cwd+'/')
        return block

    def gen_html_text(self, html, settings_list, ind, text):

        [font_style, font_size, slide_color, text_color] = settings_list

        if '<img' in markdown2.markdown(text):
            #make a split slide
            path = self.path_cwd + '/add_split_slide_css.txt'
        else:
            #make a normal slide
            path = self.path_cwd + '/add_slide_css.txt'

        filehandle = open(path, 'r')
        slide_css = filehandle.read()
        div_text = 'slide_{0}'.format(ind)
        div_text = div_text.replace(' ', '')

        new_box_text = slide_css.format(slide_color, text_color, font_style, font_size*0.75, div_text)


        html = html.replace('border-box;\n}', 'border-box;\n}  \n \n' + new_box_text)

        self.html_text = html
        self.slide_type = self.slide_type + [div_text]




if __name__ == '__main__':
    app = wx.App(False)
    launch_frame = Launch(None, (575,575), name = 'launch')
    app.MainLoop()

