#{4} {{
  display: grid;
  width: var(--width);
  height: var(--height);
  background: {0};
  color: {1};
  font-family: {2};
  font-size: {3}vmax;
  padding: 1vmax;
  grid-gap: 10px;
  object-fit: scale-down;
  grid-template-areas: "head"
                       "st"
                       "main"
                       "foot"
                       "number";
  grid-template-rows: 1vmax 1vmax 1fr 2vmax 0.5vmax;
  grid-template-columns: 1fr;
}}

#{4} > header {{
  grid-area: head;
  text-align: left;
}}

#{4} > st {{
  grid-area: st;
  text-align: center;
}}

#{4} > left {{
  grid-area: left;
  text-align: left;
}}

#{4} > right {{
  grid-area: right;
  text-align: left;
}}

#{4} > footer {{
  grid-area: foot;
  text-align: left;
}}

#{4} > number {{
  grid-area: number;
  text-align: right;
}}