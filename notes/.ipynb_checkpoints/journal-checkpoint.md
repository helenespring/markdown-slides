> 10-3-20


Some steps:
* convert markdown to html
* organise this with css
* make a standalone app gui (acts like a browser, cause html)


 For conversion of markdown to html in python, seems like several open-source projects exists. The first google result is for [markdown2](https://github.com/trentm/python-markdown2). Seems pretty easy to use.


Perhaps it will not be as hard as I think it will be to make this program! All the more reason why it should be open source, and not cost 35 euros! 





Okay so progress so far: a markdown text is broken up into chunk following a certain separator ('---' here), and each chunk is converted to html. That's in markdown_to_html.py.


I guess the next steps are to make this html render into a text that one can see, and formatted in a certain way. My gut feeling would be to try making a lil' GUI first. As for how to do that, I'll investigate it tomorrow.


11-3-20





Tried looking at tkinter, but it made me mac crash a bunch. Another option is wxPython, because it's cross-platform and runs natively on mac, which is definitely a step up from Deckset (only works on mac)! So let's take a look at wxPython.





wxPython is looking and working great! The one problem that comes with this process is not being able to run the test code in jupyter notebooks, but fortunately I had PyCharm already installed.

gui.py contains the test code. So far using the tutorial code, we can already make a text editor and open markdown-extension files! I'm over the moon <3


That's one part of the gui: editing markdown text. The second frame of the gui (out of two active frames), is the ones where slides appear.


The project can now progress one of two ways. First, make the slide gui with some blank slides or sample text, then work on connecting the two guis into one. Second, find out how to convert the text from the first gui into html, then work on the second gui directly from that output.

The first step, in any case, could be to find out how we can make a gui with two frames.


Huzzah! It's as simple as adding another instance of the MyFrame class. There should be a way to initialize them in different spots, might be nicer. 

This is also done quite easily :^D the size of the frame is also something you can pass, which comes in handy. A list of all the arguments you can use to wx.Frame are [found here](https://docs.wxwidgets.org/2.8.12/wx_wxframe.html#wxframe).


Now, there must be a way for the frames to communicate information to each other. One is the input frame, and the input to this frame is used to create the output for the other frame. It should not be extraordinarily complicated to achieve this. I thought it relied on two separate processes updating each other, but it really comes down to a single process.

The next milestone should be: enter something in one frame, and have something appear in the other one!


> 14-3-20


Let's see.


The first step, actually, will be to link the two frames, as they each have their own instance with their own menu buttons, whereas I want to control them both together.


The children of a window (in wxPython, window designates any visible object on the screen) are all destroyed once the parent is exited, so you don't have to exit them yourself.


Info about window deletion can be read [here](https://docs.wxwidgets.org/2.8.12/wx_windowdeletionoverview.html#windowdeletionoverview).


Ok I've devised a way to close the entire program from the child as well as the parent: so that's good. However, this is only from the file menu: it would be perfect if this could also happen using the lil' 'x' symbol on the frames.


Excellent! Now it is done <3 I achieved this by using wx.FindWindowByName(), with the name of the object given as a str: the object is successfully found and the whole program closes <3


Okay, _now_ we can think about how to connect the two. The easiest and most relevant goal would be to figure out how to input text in one window, and have it appear as a non-editable visual in the output window.


Yes! It is now complete, but it was a learning curve. Long story short, event handlers don't distinguish between text being changed in one window and the other window: it's the same event! So now we have our goal achieved :heartsineyeemoji:


Ok, next up: converting the text to HTML and making slides! This might be a doozy, as we have to choose which type of frame to use.


There's something called simple html list box, but I can't seem to get it to work.


This part constitutes a big challenge: google isn't showing anything!, surprisingly. Seems like no one thought to use a python GUI-maker to make slides. Indeed, Python is probably not the best suited to deal with this kind of issue. However, it's worth a shot. Let's describe here what we want the output to look like:
* segmented into slides with a default shape (but the shape can be modified via a menu)
* the text is accepted as markdown, processed to HTML, and displayed as formatted text on the slides
* when the frame is dragged, the slides expand or contract to fit the frame while still preserving its ratio (idea: only let frame be dragged by a certain ratio)


~~An idea that comes to mind: create a vector graphic of the slide? So all amounts of zooming in will preserve a nice appearance.~~ cursed idea...


As for the conversion of markdown to a formatted text, let's have a look.
* markdown --> HTML
* put HTML in CSS block
* render CSS


[this](https://pypi.org/project/cssutils/) is a resource that parses and builds CSS, but does not have rendering.


[Over here](https://stackoverflow.com/questions/10358998/wxpython-webview-example), someone mentions using a web browser to load the slides. I think this could work out! You can't really tell it's a browser, and the HTML is formatted.


Things that need to be adressed with this:
* CSS + HTML should be rendered correctly (to display all the slides, then single slides (toggle view))
* Figure out how to tame wx.Dialog (eg. can't rescale the window atm, hopefully this can be solved. Also kwargs etc. I'm not sure about how to proceed)


Found out how to add multiple styles [here](https://docs.wxwidgets.org/2.8.12/wx_windowstyles.html#windowstyles), interesting!


Yay! I now have replaced the read-only output by the browser, and everything is working as planned so far <3 The next steps will be:
* writing some CSS formatting ahead of time and then just updating the html text from the input, and making the CSS formatting evolve as new slides are made
* add a function that converts markdown to html to intercede during the update text part: this part is done, I just need to integrate it. I think it would be neater to slot it in directly into the class definition as a methods.
* maybe change the update method to text again, since it's not related


amaaaazing <3 now you don't need to switch backwards and forth! we should check whether that's good or not, since the whole thing is re-running each time. So, I guess it's worth thinking about once the entire CSS chonk is ready to be interpreted, as well as the conversion of markdown to CSS etc. I predict some for loops, that's all.


Seems like it doesn't detect new lines, that's a thing that'll have to change. Plus, there are more rules to be obeyed here, like the '---' stuff and the creation of slides. There are a few steps here:
* Make the slides appear using CSS stuff
* Make a proper markdown to html thing that interprets the creation of new slides as well as new lines.


New lines for markdown seem to not really be a thing, so we need to come up with one of our own. It all depends what the content of the input text is. If nothing appears, then we need to make it our business to insert something when the enter key is pressed, like backslash or something.


YAY! Success. This is accomplished by using the TE_PROCESS_ENTER style, which can be multiline, once \n is entered as a command ^^


> 15-3-20


Right, so today we'll focus on making the slides.


So I'm a little perplexed as how to proceed. I think this could be the only way to proceed:
* each slide is an object
* the output window has the capability to:
    * display these objects in a grid
    * display each individual object one at a time
    * launch a slideshow controled by key presses


The objects created are blocks of formatted text. How to make them objects that the program can understand? Also, what type of output window will they be stored in?


An additional thing: tab (indent) is not recognized. That could be something to keep in mind. Maybe I can make a new style based on wx.TE_PROCESS_ENTER, since the source code is probably out there.


I could do a really stupid thing, where each new slide generates a new browser! That would make it very hard to track corrections though, I think, plus it's really stupid, so let's maybe forget it.


[This example](https://tympanus.net/codrops/2017/07/19/css-grid-layout-slideshow/) shows that something called "CSS grid" can possibly be what we're looking for! Let's investigate :starsineyeemoji:


Oh no... looks like there's [a lot of reading](https://tympanus.net/codrops/css_reference/grid/) to do!


Ok, seems like we can combine CSS into HTML files in 'style' brackets: great ([here's](https://www.quora.com/How-can-I-merge-CSS-and-HTML-codes) how to do it)! So we have an html file, but we should dynamically generate it in the class, to add items and stuff.


So here's how the file is updated:
* if '---' is entered, convert that to a new slide being created
* as text is edited on different slides, update the corresponding slide.


That second one is a bit of a doozy! But the first one should be achievable right away.


So we should save the html file as a string as an attribute, then edit that string as the program evolves.


Ok done! Both are achieved, but yes there is a for loop. So there is a danger for slow-down there, but we'll see.


Great so far. Now let's consider style elements like offering options in the menu to change font size etc. Then also let's check how to format inserting images!


Ah, first, we need to make sure we insert a \n where the insertion point is! Instead of at the end of the file. Konnechno.


I'm displeased now, because whereas I now have a local insertion happening, the \n is not showing up in the markdown text being passed to the markdown converter: it just doesn't exist! So, tragically, I have to insert br\n ;\_; oh well.


On to other tasks, i.e. the style elements options and inserting images.


Here's a general overview of what remains to be done:
* ~~when slide editor is resized, resize grid & font size, ensure only vertical scrolling required~~
* ~~devise workflow. e.g. by default: open just editor, or just output, or both? Prompt opening a file/ starting a new file/ launching a presentation directly? Also devise a way to toggle between these after the initial startup.~~
* ~~ensure input is always in the foreground (otherwise annoying)~~
* ~~devise a way to launch presentations~~
* ~~export slides to pdf as other slides are exported~~ (HTML instead of PDF: for now. (see 4-4-20)
* ~~figure out how to format other markdown things (images)~~
* ~~create slide styles and a way to create templates~~


> 16-03-20


Right, need to adjust splitting of the text from '---' to '---br': done.


Also, let's remove the close and maximize options from the output window, so it's not closed ( no reason for user to do that ). Plus it looks nicer (on mac os at least it hides the fact that it's a browser ;v)


The converter seems to understand adding images, but cannot seem to locate where the file is? Let's try.


Treating the html text directly, by inserting the working directory to the start of src. Strange that it doesn't recognize the current working directory, but that's the way it is. 


Let me add the list of goals:
* have the picture size scale with the slides while conserving the placement and aspect ratio.


In order to adress this and the resizing of the slides with the screen, perhaps setting percentages instead of fixed pixel lengths and widths could be the key here. Don't know if that's particularly kosher but it could be a possible implementation.


> 18-3-20


The zoom capability could work as well.


> 20-3-20


There should be a middle ground. [This example](https://codepen.io/anon/pen/dWoZjb) describes how to make grids scale with screen, which works well. However, it doesn't scale in proportion exactly, might require some tinkering, or looking somewhere else.


> 21-3-20


[This example](https://codepen.io/danield770/pen/bjYvOj) seems totally perfect, but for one thing: the box expands with the text. Ideally, this would be stopped. We'll have to devise a way to stop this from happening.





> 22-3-20


Building on yesterday's example, the font is made to scale with the window size as well as the images, by modifying the number of slides in a row to be set to 3 instead of being automatic. The font is then scaled with the window, and all is well.


Now, the issue that remains with this example is the size of the grid expanding as text is added. We need some kind of textbox feature, with a text wrapping option, to make it go to the next line when we need to. This will become important when we want to insert images to the left/right.


RUH ROH! Now it seems like headers aren't being formatted correctly!?!!? Shit. All the more reason to make a proper text box.


Interesting: [textarea](https://www.w3schools.com/tags/tag_textarea.asp) (and [example](https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_textarea) ) lets you insert text and resize the textbox. That could be interesting. The idea for this project, however, is to convert markdown to a finished set of slides.


So here is a list of the immediate goals:
* ~~Format and comparmentalize the text based on markdown image instructions~~
* ~~Work out image insertion based on ditto.~~
* ~~Ensure that the slide doesn't expand!~~
* ~~Fix headers and stuff.~~


Okay! [This example](https://stackoverflow.com/questions/49186981/word-wrap-not-working-in-a-css-grid-item) shows how to wrap text to not exceed the box size, so that's good for the left-right text. Would be nice be able to set when it starts to wrap. I think it's defined by the size of the element, so perhaps a second element is needed? That has padding coming from the left or right? Hm. 


The grid stretches to adapt to two things:
* the size of the viewport, which is what we want
* the content of the slide (in the vertical direction), which is what we don't want!


Ideally, I would like to find a way to authorize overflow beyond the slide. What we could do is come at this from the python script side.


If we could have two objects: a box, containing all content (images & text, and the slide background), that is allowed to scale in height, whereas the slide background would be unaffected... hm.


By looking at [this](https://www.joomlashack.com/blog/tutorials/css-grid-12-the-minimax-function/), it's pretty clear that the row will fit to accomodate the size of the content. So, it's clearly up to us python-side to intervene. ALTERNATIVELY, we can leave it up to the user to know when to stop.


I think this idea of having several objects within a grid element is nice. Like a sub-grid. This would make formatting easier, like align left and right for the images.


Images can be made to scale to not resize the grid. Sad that this is not the case for text ToT. So would there be a way to convert text to image or something!?!?! Alternatively, if I could access a content-height thing, then that would be dope, as it could instruct the down-scaling of the font, which would be the nicest solution.


Alternatively, I can calculate the screen resolution in pixels, then convert vw's to pixels. This is done [here](https://codepen.io/vikrammehta/pen/vgmGPN)! So let's see if we can make justice prevail.


[OH SNAP](https://css-tricks.com/fun-viewport-units/) Looks like I unexpectedly found what I was looking for! Perhaps! Let's get to work ^^ Yup: it checks out! It looks amazing ;v;


Ok, now we can worry about how to separate the slides in two. This would be done upon insertion of an image with either the prescription "right" or "left". Let's first check how the image insertion goes for this current build.


We can include image size scaling by modifying selected parts of the html file, namely the img { } part: the image height has a certain factor, so that should be easy to implement.


Now, how to force a text wrap at an artificial border (namely, the halfway point).


There may be a way to make subgrids as outlined [here](https://gridbyexample.com/examples/example21/). In order to do that, first I have to divide the text into image specifiers and other text. That seems a little tricky :^K Do we split the text again? We'd have to do it twice, in order to get text before and after the image.


So I'm doing that now, not working yet but hopefully I can make it work soon.


> 23-3-20


[This](https://medium.com/@PavelLaptev/css-weekly-2-an-image-on-the-left-text-on-the-left-a5e59b2acdb5) is closer to what I'm interested in. However, I can't seem to get it to work. It's called SCSS instead of CSS: hopefully there's a way to use it. I can't see a simple way to use it: uh oh. Maybe we can't use it then! [This](https://codepen.io/danield770/pen/bjYvOj) example is the key, and using it looks good. Just need to figure out how to wrap the text along half the slide.


! Ah, it seems like the entire text wrap function has been overwritten! Better look through the git history.


Oh no... I can't figure out why the text isn't wrapping... hmmmm Oh it just doesn't wrap one long word. All right. I am pleased with the results just far! To make this complete, I just want to ensure that headers h1 are centered. The trouble is that it aligns with the half-text. We can leave that for later.


What we need to fix is the position of the left/right images, because they're at the top of the page.


Ok! I fixed both the header and the image problem (I believe) by using relative positions. Might need some tinkering, but I'm pleased with the fixed!


Excellent! What remains to be done is the fun stuff now <3 enough struggling with CSS!


![](journal_illustrations/party-time.jpg)


I should come up with some ideas for the name <3 I'm drawn to mythological creatures, and/or something to do with water (like "wave" or "splash" or something fun like that). Maybe a constellation ^^ Otherwise something lively, like "lithe" or "berry" or smth


> 24-3-20


Okay, the inspiration isn't coming to me in a flash. So, I'll proceed with other things while thinking of what to call it and the icon n stuff <3.


Let's start by organizing the workflow. We have a start screen that 
* opens on: 
    * opening the program
    * editors closing
* closes on:
    * editors opening
* used to:
    * open editors


The Deckset and powerpoint start windows have clickable templates, and deckset has clickable previous files with a preview slide. That could be quite fearsome to execute: back to the CSS?!?!?! arg. It wouldn't actually be so bad. It would, however, involve keeping a record of all files created with the app. In doing so, we could also save:
* the working directory of that file
    * in case there are images in that slide
    * in order to open the file, perhaps
* the information contained in the first slide
Where to save this information? I guess in the directory where the executable file is. So, perhaps I should give some thought about making this. 


For macs, to put an app on the app store, you need to build shit with swift. & even just to build an app for mac os, you need to do that. For now, it's not hyper necessary. What we can do is ask people to clone a repo and run the main py file from there.


For now, let's proceed by preparing the launch window & companion files.


I think making a dialog would be the simplest option here.


I think I will call it Splash: has a nice ring to it ^^


LOL it's actually so much worse to not use CSS and HTML to style the dialog page X'D I think I got stockholm syndrome'd, but wxpython just has the shittest documentation.


So we're back to having an option to create a new presentation, and have the other presentations listed, _in order_. This is going to involve some thought, but it shouldn't be completely impossible. Moreover, it might not be necessary to reinvent the wheel completely.


The question is: what's the idea.
* If a .md file is saved, add that file and its path to a file_history file. Could be stored as metadata. The metadata stored should be:
    * name of the project
    * filepath (absolute)
* Upon preparing the launch screen, the following should be queried:
    * the content of the first slide
I think it would be simple to do this using e.g. YAML to store the metadata.


Let's try doing this.


In order to fulfill this, we would have to be able to capture an event like ON_SAVE or something.


Saving can be done via the menu, but it should also be possible to do it via CTRL + S. Someone shows how to make wxPython understand key shortcuts [here](http://www.blog.pythonlibrary.org/2010/12/02/wxpython-keyboard-shortcuts-accelerators/).


Okay! I did a bunch of stuff just now, including handling saving of files. Not complete yet (should prevent from closing without saving, and also prevent overwriting other files without checking), but good so far. Also, I've added a simple way to keep track of the file history ^^ (dictionary with the path as the key, so that there's no overwriting). Another thing to do would be to organize the files_dict by last opened, to make things practical.


Now, we have a few immediate goals:
* connect the file history to the launch screen
* make the css grid connect to hyperlinks that open the editor, and open the file / blank presentation.


Okay ^^ The launch screen is showing the first slide of each of the presentations in the file_history, and has some FileNotFoundError handling to boot :^}


So how to connect the launch screen and the editor in the simplest way possible? Since we're dealing with html, I think about hyperlinks. Since we're also dealing with a wx.Dialog object, I'm thinking of buttons. Both of these things respond to clicks.


We can perhaps detect an intention to navigate, then react. Yes, the EVT_WEBVIEW_NAVIGATING thing works. Found a small implementation of it [here](https://python-forum.io/Thread-wx-webview-event). We further are able to fetch the target URL, which is where we can choose to smuggle the path of the file we wish to open.


Huzzah! The launch screen and the editor are now connected <3 and it's lookin' great!


Okay. We can now think about how to switch the view between the grid and the single slide. Then, comes the major point of launching presentations. In the meantime, error handling should be added to the saving, and perhaps a way to sort the recently modified files.


For the last point, a datetime stamp could be conferred to the dictionary rather than a name. The name of the file isn't used anywhere, I don't think, and it can be obtained from the path directly. 
Error handling I can think of atm is: prevent closure without saving, prevent overwriting files. These should be easy to implement by this evening.


Overwriting is done naturally during the save as function, so that's good. For closing, let's just have it save on exiting.


Ok! Chose faite. Now the last thing we can look at is the sorting of the file_history by its datetime values.





Fatto!


> 25-3-20


Nice!


There are four main items to think about:
* ~~How to switch between single slide and grid view~~
* ~~How to launch presentations~~
* How to export slides to PDF
* How to format fonts, background color etc.


For the customization, it can be a menu option that opens a dialog w/ preview. We can also have templates, and the possibility to save templates.


Xhtml2pdf seems to bug out. Weasyprint also.


Aside: I should be keeping track of the dependencies! uhuhuhuh and making a note of those.





> 26-3-20


Idea: look into "print" function of browser to generate pdfs?


An important thing to do would be to ensure that more than one image can be inserted, and how.





> 28-3-20


Let's set the editor default H&Ws.


! We should get the input frame to wrap the text! For this, we need to revert to multiline controls, and process enter internally!


Okay ^^ I had it in me mind that wx.TE_PROCESS_ENTER and wx.TE_MULTILINE were mutually exclusive. Their documentation on that front was a little confusing to me. Now maybe I don't have to mark br! Let's see ^^


Nope... well... anyway. It might be possible to store the text in another object, and add \n to that, which would then be sent to the update text thing. Yup, that could work & be a nicer solution. This could get quite ugly, but it's worth a shot. Actually, it was really easy! The problem was coming from markdown2, apparently! Left \ n as \ n, so now I just change it to br before splitting the text. EZ.


We should also have the slides render on opening a file, cause they just sit and wait for a key entry to render for now. But that doesn't trigger an event actually. Binding window creation to Update works ;)


Hooray ^^ Perhaps making the grid slide view different, and have sans-serif text would be nice.


Hm this saving on exiting is worse than not saving on exiting, as I've totally edited my file! I'd better change that. Alright, now there is a dialog that prompts you to save your work if it is changed (seems to be able to detect that perfectly).


I would now like to figure out how to toggle between slides. Here are the features I would enjoy:
* ~~Ability to zoom on one slide at a time, and scroll between them~~ (this could later be used as a jumping-off point for presentations) using arrow buttons and clicks.
* ~~Ability to pull up the slide being edited on screen, in the single-slide view.~~


I suppose this means that the grid viewer should be in 16/9 aspect ratio (can it be set to be dragged in a way that respects that?), so that the switch between the two views looks perfect. 
... So it doesn't look as though that's an option? Here are some options:
* Have two windows, both visible
* Have two windows, only one visible at a time.
    * ~~Recall position, size options of grid window~~
    * ~~Switch between single- and multi- view via wxPython-recognized event~~
    * ~~Use position for single-view window, but force a 16/9 aspect ratio size, using the width of the grid window as reference.~~


Methinks I'll try the second option, sounds feasible.


I think a button-press + a hotkey toggle could be quite nice.


From multi-view to single-view:
* ~~First option: 'view' menu, instantiates on 0th slide.~~
* ~~Second option: Click on a slide. When navigating, the path is the index number of the slide position, so the single-view should exhibit only that slide based on the index.~~
* ~~Once in single-view, editing proceeds & it is necessary to detect _where_ the text is being edited (i.e. on what slide). This could be done by detecting how many === are before the current position, in order to get the index.~~
* ~~Registers left arrow key and right arrow key presses to move the index up or down.~~


From single-view back to multi-view:
* ~~'View' menu~~
* ~~... and a hotkey.~~


Ooo setting the page and updating the text counts as navigating! We need to think of another webview event..


Hmmmm I think it would be best to switch views within the same output window, since the text is sent to the original output window. Ok, it is done! Navigating using the left/right keys is also done, using accelerator tables.


Now, let's think about how to switch back to multi-view. Ok, all done!


Alright, that's enough work for this commit. The next thing we can think about is how to launch a slideshow!


> 29-3-20


! Now, the slides aren't adapting to the browser size! Yo wassup.


Seems like changing the size and pos is messing it up, so I removed it. Since there's only one output window, they're pointless things to manually measure anyway. Ok, we're back on track.


As for the aspect ratio, 16/9 corresponds to fullscreen on my computer, so the slide fits the screen perfectly. In order to launch presentations, we should force full-screen (exit with escape) & set the background to black.


Maximizing the output window doesn't exactly solve the problem, as it's not a 'fullscreen' thing, so the computer toolbars are visible still. Also, the font doesn't seem to scale appropriately: a investiguer.


How to go fullscreen? [This post](http://www.blog.pythonlibrary.org/2013/07/12/wxpython-making-your-frame-maximize-or-full-screen/) talks about that. It's good, but yea the font needs to scale properly.


So now we can enter and exit fullscreen, and we do this via a hotkey-activated toggle function.


Problems with the fullscreen:
* ~~The input editor is above the output frame: remove this.~~
* ~~Slide is glued to the top of the screen (Maybe have a separate html file if fullscreen is activated)~~
* ~~Font not scaling correctly.~~


[Here](https://stackoverflow.com/questions/31217268/center-div-on-the-middle-of-screen) is where I saw how to center the slide.


What remains now is dealing with the font scaling.


Looks like the font scales well in the multiview, but not in the single view, so that's a clue.


Ok looks like we're good for the fullscreen! Let's push it.


Now, we can think about how to add customization to our presentations. Things to customize:
* Background color
* Background image? (think about how to set an image as a background btw)
* Font style
* Font sizes
* Global slide formatting (eg. bar for logo)


Can we get dialogs from dialogs?, because there is a [separate](https://wxpython.org/Phoenix/docs/html/wx.ColourDialog.html#wx-colourdialog) dialog for color picking, a [dialog](https://wxpython.org/Phoenix/docs/html/wx.SingleChoiceDialog.html#wx-singlechoicedialog) for making a selection from a list (font).


Ideally I would like to combine them together, but might be hard seeing as wxpython is hard to find examples of.


[Buttons with symbols](https://pythonspot.com/wxpython-buttons/)


[Here](http://www.blog.pythonlibrary.org/2010/06/10/wxpython-a-tour-of-buttons-part-2-of-2/) is some info on how to link specific buttons to specific actions.





31-3-20


Ok! Now we have functioning dialogs for text and slide colors, and font style and sizes. What we need to think about now is how to preserve this information (it's in the CSS, not the markdown.)


An option would be to create a companion CSS file for each markdown file, where color preferences etc. are stored.


Additionally, we should think about how to present templates and allow users to create their own templates.


> 1-4-20


Christian mentioned global variables, that could be a thing to consider. Alternatively, it's also possible to make a file extension (.sh?). Or just generate files around the original markdown file (same path), but that has its problems if the file is then moved around. I guess what we can do is save it with the same name but with a HTML extension. Or file_name_slide_appearance.html. ALTERNATIVELY, we can save it in the file history! Yea, that's a great idea. We'll need it anyway to prepare the slide preview.


So it's settled then.


Okay! That's now how it works. We just need to do something special for the launch screen: defining a single html file, but appending new box class to it, with its own styles, for each file.


It's being implemented atm, couldn't debug it by today: #TODO tomorrow. 





> 4-4-20


Okay! Now the launch screen displays the first slide of each project with the correct format. I was all pleased with myself, but then Christian said something about having an "UNDO" function: indeed! 


I'm looking around online, looks like there isn't an immediate thing for doing this. [There is](https://wxpython.org/Phoenix/docs/html/wx.CommandProcessor.html#wx.CommandProcessor) something called CommandProcessor, but it looks weird. I have an idea on how to implement my own undo/redo thing, but it's gonna slog down the program. Perhaps I'll leave it for now, and come back to it later.


For now, let's finish up the previous points, namely exporting files as pdf and making the slide customization nice. On the topic of the first thing, Anton mentioned something: phantomjs. The first google search suggestion for phantomjs is "phantomjs alternative", so that sounds promising ahaha.


Seems like phantomJS is no longer supported, so I'll try selenium + Chrome instead. Hopefully that can be possible.


That seems to cause too much trouble. Let's take a look at pdfkit.


OMG it requires downloading programs. Goddamn. How hard is it to render html and take a screenshot!?!!? Jeez. I can cask install it w/ mac os... I don't like generating all these dependencies. Akh.

```python
import pdfkit
import os
import codecs

path=os.getcwd()
html = codecs.open(path+'/launch_html.html', 'r').read()

pdfkit.from_file(path+'/launch_html.html', 'output.pdf', css = path+'/css.css')
```

Ok so it works, but the resulting image is... let's say... slightly underwhelming. None of the text formatting survives!


Selenium and the headless browsers Chrome etc doesn't want to cooperate. Let's move along and try to find something worthwhile.


Looks like the html renders correctly in all browsers, so actually why don't we export to html? Lol.


I'm giving up because it seems that support for CSS grid to pdf hasn't yet begun, at least for [wkhtmltopdf](https://github.com/wkhtmltopdf/wkhtmltopdf/issues/4649) (and pdfkit uses wkhtmltopdf). Getting headless browsers to cooperate also seems to be an issue, go figure why.


When exporting:
* the slides should form a single column 
* the links on the slides should be removed ( a --> div )


The disadvantage of not using pdf is not being able to flip through slides using the arrow keys. However, that's the way it'll be for now.


Okay! We can now export to html: wonderful.


Now, we can make Splash pretty <3


Oh my god I've drained so much into this time vampire: fuck you, wxPython, your git repository makes me cry acid tears. I'm currently displeased with the lack of prettify-ability here.

> 5-4-20

Important thing to do: make it so that two images can be on a slide together.






Also, headers don't work the way they're supposed to due to the way the text is split up, so I have to catch them myself.


Note: ~~change how paths are treated for images, so make sure that a file path is always communicated to the Output class.~~


I'm running into more difficulties than I expected for displaying images. This will take some further thought at a later time.


> 10-4-20


For displaying the images: perhaps a sub-grid would be a good idea, although I don't believe that's implemented as of yet.


For Deckset, I now recall that you can't write on the same side as the image. So let's discard this as well. All text is confined to the side the image is not on, and if two images are entered, no text can be input apart from a header.


Okay that's settled. How will footnotes affect this? !footnotes need to be parsed correctly! 


The markdown2 stuff for footnotes works, but doesn't slot in well after \ n is converted to < br >, and this conversion is problematic after the markdown2 conversion. As such, it's probably best if we come up with out own footnotes formatting. This opens up a pretty troublesome case, actually, as we want to insert text at the bottom of the screen. Back to CSS! lol of course it is.





9-5-20

```python
text = 'quote this [^1] some text \n [^1]: it\'s cited now. ===\n how about this? [^2] well let\'s see \n [^2]: yup.'

# have to process headers ourselves
split_text = text.split('===\n')
for ind, text_chunk in enumerate(split_text):
    if '#' in text_chunk:
        # first enter should be precisely between the header and the body
        first_enter = text_chunk.find('\n')
        if first_enter != -1:
            text_chunk = text_chunk.replace('#', '<h1>')
            text_chunk = text_chunk.replace('\n', '</h1>\n', 1)
    split_text[ind] = text_chunk
text = '===\n'.join(split_text)

# #process footers before splitting the text
split_text = text.split('===\n')
for ind, text_chunk in enumerate(split_text):
    text_chunk = markdown2.markdown(text_chunk, extras=['footnotes'])
    split_text[ind] = text_chunk
text = '===\n'.join(split_text)
text = text.replace('<br>','')
text
# replace \n with <br>
# text = text.replace('\n', '<br>')
# split_text = text.split('===<br>')
```

Footnotes are now parsed directly by markdown2.markdown, but that creates a horrendous format. So, we need to deal with that ourselves and put it in our CSS, which is reasonable.


[This](https://stackoverflow.com/questions/46158844/how-can-i-have-a-sticky-footer-with-my-css-grid-layout) has a great way to keep footer text (as well as header text!) confined to certain areas. This gives me the idea to further split the main area of the slide into left and right, as well as keeping a part for the page header. Could also add in a slide number part at the bottom!


I learned about grid-template-areas [today](https://developer.mozilla.org/en-US/docs/Web/CSS/grid-template-areas#Example), and it should accomplish all of the above! The last thing to do will be incorporate headers and footnotes properly lol


So far sorting into left, right and main is done. I need to make so that headers go to headers, titles go to titles, and of course... footnotes to footers... 


Okay, all that remains is footnotes to footers. A problem for future Helene ;v


>10-5-20


So with the markdown2 treatment, the footnotes are sent to their own class. It should hopefully be possible to rewrite this class to the appropriate contained without worrying about the other additional shit.


Alternatively, I could make it so that citations are easily collected and don't rely on markdown2. For example, it could be: [^1](the citation)


Would be changing how markdown works, exactly, but would be the easiest thing to do.


OK, let's try that.


Yup! It's now finally complete ^^ everything goes to the correct area this way, without too much trouble. I just need to add slide numbers (format them as pale gray, methinks). Also citations as gray, perhaps.


Or, extend the color choices to this, and default is black or something else.


Now, we need to check how double images are implemented and whatnot.


Also, I don't believe the inclusion of a single centered image is handled actually!


! the viewer snaps to the top of the page as the text is being updated. Could have something to do with the page being refreshed. I don't remember if that was a thing before, but it likely is.


Also, the larger and presentation view aren't working. ALSO, the launch thing now isn't representative of the slide cause it's separate html. ALSO ALSO, headers and titles aren't cooperating fully for multislide view.


So, here are the #TODOs for next time:
* ~~make single view work ~~
* ~~make launch view work~~
* ~~make presentation view work~~
* make headers and titles cooperate
* make a single image be included centrally with some accompanying text or whatnot

Further #TODOs:
* make color selector etc look nice & offer different text color options for headers, footers, main text etc.
* add option to include slide numbers
* make citations cooperate with images


> 16-5-20


In order to make views apart from multiview work, they have to follow the same markdown to html and css styling as multiview. Let's check the latter first.


Unfortunately, now the presentation slide now floats near the top! There must be something I removed that was of importance.


Ok put it back (in the grid specifications). Now, we're missing some adjustments to make this perfect: the slides are a little too wide perhaps due to some grid spacing changes or whatever, so we need to tweak them to fit the screen size.


For the single view, there's this whole thing. Even though the grid is specifically set to 9/16 ratio, a window with 9/16 ratio isn't good enough. ! I think the window part of the screen is what's wrong! maybe removing that altogether would be good.


! the problem with this is that it can't be dragged around! ofc and alas. so i just have to factor in the things


The scroll bar kicks in or disappears & it seems like the size can't be set independently in x and y for some reason.


window.innerwidth etc. is a working draft atm, which is unfortunate, but here we are. I;ll fix this later then.

Future #TODO: 
* fix aspect ratio problems for single view


Ok, now let's fix the launch view.


Done! That took a dang ol' while.



